let
  eric-t470 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDppXzPalgkxgaEnabqirf2sbx1LJW60jpjK0XSoLMa/ylnR8e0fHSYhU3lxaSxswUkMV71USt/7mtSWSu0suQEpqAocon55xCBFa6pmDSWae2196WfQokmJFHYVDbt/LmmI38/1zLOCmqAXOEqjfc/ggXaKnO/FTwp+Ie+MrlaHYFCJemNWSbLH4mrrBpT3R7BZVgHePUMcoiZF0ZWMwcYOlGOk4rd6xsz7JxKAS65GOp50n3nwdtMb8BCyy/0ZpELNC8i9JxomDeuOQl8czPn1OFvhEi/j6OGt3Ycu85uYQDqE3ORP92LVwm7Pj+iJxDaxyTmAMMsbVhl5MJRS5j5GrftKfVbonUcXEdlKQ/lTtlAXQ7Sp6NaylryMcb3Ko9/ZqOQdXZZf57d3qgoz4wuNEvRjBVXA+77HHBNstR996+G0s4wgwx394IJ11i7nAELqNSNAblOBAEyGKssF60F7TcqT1ZLVD7b+4AHrqNSSnup3GePPREIXcG7FcgT0+U= eric@eric-t470";
  apps-server = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOLZmABXjwaKlPNmUYuGu9QGcHkuQmZ+ves/YvwDjG0h";
in {
  "freshrss-passwordFile.age".publicKeys = [ eric-t470 apps-server ];
  "eric-hashedPasswordFile.age".publicKeys = [ eric-t470 apps-server ];
  "root-hashedPasswordFile.age".publicKeys = [ eric-t470 apps-server ];
  "node-red-settings.age".publicKeys = [ eric-t470 apps-server ];
  "restic-password.age".publicKeys = [ eric-t470 apps-server ];
}

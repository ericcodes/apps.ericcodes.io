{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";

    systems.url = "github:nix-systems/x86_64-linux";

    utils.url = "github:numtide/flake-utils";
    utils.inputs.systems.follows = "systems";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, utils, nixos-generators, agenix, ... }:
    {

      nixosConfigurations = {
        # The configuration for apps.ericcodes.io
        app = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./nixos/providers/gandi/gandicloud.nix
            agenix.nixosModules.default
            ./configuration.nix
          ];
        };

      };
    }
    //
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };

        # src: https://github.com/OliverEvans96/gandi-nixos/blob/bda434ade4e26e56af992af2ea8b612cfc2038c5/flake.nix#L15
        mkShellApp = body:
          let script = pkgs.writeShellScript "script.sh" body;
          in {
            type = "app";
            program = "${script}";
          };
      in
      {
        apps = {

          # This copies the Gandi VPS configuration from the server to the repo
          sync-gandi-provider = mkShellApp ''
            ${pkgs.openssh}/bin/scp root@apps.ericcodes.io:/etc/gandi/configuration.nix nixos/providers/gandi/gandicloud.nix
          '';

          # Deploy the configuration to server
          deploy-as-root = mkShellApp ''
            ${pkgs.nixos-rebuild}/bin/nixos-rebuild switch --flake .#app --target-host root@apps.ericcodes.io
          '';

          deploy = mkShellApp ''
            export NIX_SSHOPTS="-o RequestTTY=force"
            ${pkgs.nixos-rebuild}/bin/nixos-rebuild switch --flake .#app --verbose --use-remote-sudo --target-host eric@apps.ericcodes.io
          '';
        };

        devShell = with pkgs; mkShell {
          buildInputs = [
            agenix.packages."${system}".default
            lego
            nodePackages.node-red
          ];
          shellHook = ''
export PROJECT_ROOT_PWD="$(pwd)"
export PROJECT_ROOT_NIX="${toString ./.}"
'';
        };
      });
}

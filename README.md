# apps.ericcodes.io

These are the self-hosted web applications.

## Setup

1. Create a NixOS VPS in Gandi
2. Create an DNS entry for `apps.ericcodes.io` and `*.apps.ericcodes.io` for that VPS
3. Run `nix run .#sync-gandi-provider` to copy the Gandi specific NixOS configuration locally
4. Run `nix run .#deploy-as-root` 

NOTE: `deploy-as-root` will eventually disable SSH access for root. I
want to make sure `deploy` is stable enough. 

## Deployment


```sh
nix run .#deploy
```


{ config, lib, pkgs, ...}: let
  rssHostname = "rss.apps.ericcodes.io";
  ircHostname = "irc.apps.ericcodes.io";
  qsoHostname = "qso.apps.ericcodes.io";
  node-red-hostname = "red.apps.ericcodes.io";
  ntfy-hostname = "ntfy.apps.ericcodes.io";
in {
  system.stateVersion = "23.05";

  nixpkgs.overlays = [
    (final: prev: {
      update-pota-refs = prev.writeScriptBin "update-pota-refs"
    ''
mysql -e "update TABLE_HRD_CONTACTS_V01 set COL_POTA_REF = COL_SIG_INFO where COL_SIG = 'POTA' and COL_POTA_REF = '''" cloudlog
'';
    })
  ];

  environment.systemPackages = with pkgs;[
    ntfy-sh
    update-pota-refs
  ];

  ####
  ## Networking config
  ####
  networking.firewall.allowedTCPPorts = [
    80
    443
  ];

  ####
  ## Backups
  ####
  services.restic.backups.services = {
    paths = [
      "/root"
      "/var"
    ];

    extraBackupArgs = [
      "--verbose"
      "--exclude-caches"
    ];

    timerConfig = {
      OnCalendar = "daily";
      Persistent = true;
    };

    # This requires the the public key of root is copied to the server's authorized keys
    # See: https://docs.hetzner.com/robot/storage-box/backup-space-ssh-keys for details
    repository = "sftp://u387620-sub5@u387620.your-storagebox.de:23/restic-apps.ericcodes.io";
    passwordFile = config.age.secrets.restic-password.path;
  };

  ####
  ## Garbage Collect (GC)
  ####
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  ####
  ## SSH config
  ####

  services.openssh = {
    openFirewall = true;
    settings = {
      PasswordAuthentication = false;
      X11Forwarding = false;
      KbdInteractiveAuthentication = false;

      # Uncomment this line once you feel like deploying without root is reliable enough
      # PermitRootLogin = lib.mkForce "no";
    };

    extraConfig = ''
      AllowAgentForwarding no
      AllowStreamLocalForwarding no
      AllowTcpForwarding yes
      AuthenticationMethods publickey
      GSSAPIAuthentication no
      KerberosAuthentication no
      LoginGraceTime 20
      MaxAuthTries 3
    '';
  };

  ####
  ## agenix secrets
  ####
  age.secrets.freshrss = {
    file = ./secrets/freshrss-passwordFile.age;
    owner = "freshrss";
    group = "freshrss";
  };

  age.secrets.eric-hashedPasswordFile = {
    file = ./secrets/eric-hashedPasswordFile.age;
  };

  age.secrets.root-hashedPasswordFile = {
    file = ./secrets/root-hashedPasswordFile.age;
  };

  age.secrets.node-red-settings = {
    file = ./secrets/node-red-settings.age;
    owner = "node-red";
    group = "node-red";
  };

  age.secrets.restic-password = {
    file = ./secrets/restic-password.age;
  };

  ####
  ## Users
  ####
  nix.settings.trusted-users = [
    "root"
    "@wheel"
  ];
  users.mutableUsers = false;
  users.users.root = {
    hashedPasswordFile = config.age.secrets.root-hashedPasswordFile.path;
  };

  users.extraUsers.eric = {
    isNormalUser = true;
    hashedPasswordFile = config.age.secrets.eric-hashedPasswordFile.path;
    shell = pkgs.bashInteractive;
    extraGroups = [ "wheel" ];

    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDppXzPalgkxgaEnabqirf2sbx1LJW60jpjK0XSoLMa/ylnR8e0fHSYhU3lxaSxswUkMV71USt/7mtSWSu0suQEpqAocon55xCBFa6pmDSWae2196WfQokmJFHYVDbt/LmmI38/1zLOCmqAXOEqjfc/ggXaKnO/FTwp+Ie+MrlaHYFCJemNWSbLH4mrrBpT3R7BZVgHePUMcoiZF0ZWMwcYOlGOk4rd6xsz7JxKAS65GOp50n3nwdtMb8BCyy/0ZpELNC8i9JxomDeuOQl8czPn1OFvhEi/j6OGt3Ycu85uYQDqE3ORP92LVwm7Pj+iJxDaxyTmAMMsbVhl5MJRS5j5GrftKfVbonUcXEdlKQ/lTtlAXQ7Sp6NaylryMcb3Ko9/ZqOQdXZZf57d3qgoz4wuNEvRjBVXA+77HHBNstR996+G0s4wgwx394IJ11i7nAELqNSNAblOBAEyGKssF60F7TcqT1ZLVD7b+4AHrqNSSnup3GePPREIXcG7FcgT0+U= eric@eric-t470"
    ];
  };

  ####
  ## MySQL
  ####
  # Used by cloudlog
  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
  };

  services.mysqlBackup.enable = true;

  ####
  ## NGNIX global config
  ####
  security.acme = {
    acceptTerms = true;
    defaults.email = "eric@ericcodes.io";
  };

  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
  };

  ####
  ## RSS feed reader
  ####
  services.freshrss = {
    enable = true;
    baseUrl = "http://${rssHostname}";
    virtualHost = rssHostname;
    passwordFile = config.age.secrets.freshrss.path;
  };

  services.nginx.virtualHosts.${rssHostname} = {
    forceSSL = true;
    enableACME = true;
  };

  ####
  ## IRC web client
  ####
  services.convos = {
    enable = true;
    reverseProxy = true;
    listenPort = 3000;
  };

  services.nginx.virtualHosts.${ircHostname} = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:3000/";
      proxyWebsockets = true;
    };
  };

  ####
  ## QSO Ham Contact Logger
  ####
  services.cloudlog = {
    enable = true;
    baseUrl = "https://${qsoHostname}";
    virtualHost = qsoHostname;
    # upload-qrz.enable = false;
    # upload-lotw.enable = false;
    # update-lotw-users.enable = false;
  };


  services.nginx.virtualHosts.${qsoHostname} = {
    forceSSL = true;
    enableACME = true;
  };


  ####
  ## Node Red
  ####
  services.node-red = {
    enable = true;
    configFile = config.age.secrets.node-red-settings.path;
  };

  services.nginx.virtualHosts.${node-red-hostname} = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:1880/";
      proxyWebsockets = true;
    };
  };

  ####
  ## ntfy notification service
  ####
  services.ntfy-sh = {
    enable = true;
    settings = {
      base-url = "https://${ntfy-hostname}";
      behind-proxy = true;
      # Create users using the nfty command: https://docs.ntfy.sh/config/#access-control
      auth-default-access = "deny-all";
    };
  };

  services.nginx.virtualHosts.${ntfy-hostname} = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:2586/";
      proxyWebsockets = true;
    };
  };
}
